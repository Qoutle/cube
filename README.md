# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* this is a developmental process with infinite possibilities
* Prelaunch phase ... do not install any feature [not tested yet]
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download Git , SourceTree , Netbeans and JDK 1.6^
* Import the Developmental libraries into Netbeans 
* Start Coding and Gitting 
* A JSON file would be used to fulfil the requirement of database
* Do not run tests on the main branch if you are not clear what to do please refer to admin for help  
* Try test runs from your Netbeans IDE

### Contribution guidelines ###

* Anyone can contribute read the Contributor License Agreement  
* everyone please help us review the code. Let us serve you better 
* Thanks to all and welcome to Qoutle

### Who do I talk to? ###

* Repo owner: Qoutle LLP
*  team contact: observer@qoutle.com